#ifndef __TAVLCALENDARIO_H
#define __TAVLCALENDARIO_H

#include <iostream>

using namespace std;

#include "tvectorcalendario.h"

class TNodoAVL;

class TAVLCalendario
{
	friend ostream &operator<<(ostream&, TAVLCalendario&);

	private:
		TNodoAVL *raiz;
		void InordenAux(TVectorCalendario&, int&) const;
		void PreordenAux(TVectorCalendario&, int&) const;
		void PostordenAux(TVectorCalendario&, int&) const;
		void InsertarAux(const TCalendario&);
		void Reorganizar();
		void ActualizarPesos();
		bool ComprobarPesos();
		int max(int, int) const;
		void BorrarAux(const TCalendario&);
		void BorrarPadre();
		void BorrarPadreDosHijos();

	public:
		TAVLCalendario();
		TAVLCalendario(const TAVLCalendario&);
		~TAVLCalendario();
		TAVLCalendario &operator=(const TAVLCalendario&);

		bool operator==(const TAVLCalendario&);
		bool operator!=(const TAVLCalendario&);
		bool EsVacio() const;
		bool Insertar(const TCalendario&);
		bool Buscar(const TCalendario&);
		int Altura() const;
		int Nodos() const;
		int NodosHoja() const;
		TVectorCalendario Inorden() const;
		TVectorCalendario Preorden() const;
		TVectorCalendario Postorden() const;
		bool Borrar(const TCalendario&);
		TCalendario Raiz() const;
};

class TNodoAVL
{
	friend class TAVLCalendario;

	private:
		TCalendario item;
		TAVLCalendario iz, de;
		int fe;

	public:
		TNodoAVL();
		TNodoAVL(const TNodoAVL&);
		~TNodoAVL();
		TNodoAVL &operator=(const TNodoAVL&);
};

#endif