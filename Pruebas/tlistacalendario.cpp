#include "tlistacalendario.h"

ostream& operator<<(ostream& os, const TListaCalendario& l)
{
	TListaPos p;
	p = l.Primera();
	int tam = l.Longitud();
	os << "<";
	
	if(tam != 0)
	{
		for(int i = 0; i < tam; i++)
		{
			os << l.Obtener(p);
		
			p = p.Siguiente();
			
			if(i < tam -1)
				os << " ";
		}
	}
	os << ">";
	
	return os;
}

TListaCalendario::TListaCalendario()
{
	primero = NULL;
}

TListaCalendario::TListaCalendario(const TListaCalendario& l)
{	
	primero = NULL;
	
	TListaPos sig;
	
	sig = l.Primera();
	
	while(!sig.EsVacia())
	{
		Insertar(l.Obtener(sig));
		sig = sig.Siguiente();
	}
}

TListaCalendario::~TListaCalendario()
{	
	TNodoCalendario *nodo = NULL;
	
	while(!EsVacia())
	{
		nodo = primero;
		primero = primero -> siguiente;
		delete nodo;
	}
}

TListaCalendario& TListaCalendario::operator=(const TListaCalendario &l)
{
	if(this != &l)
	{
		if(!this -> EsVacia())
			this -> ~TListaCalendario();
		
		TListaPos sig;
		
		sig = l.Primera();

		while(!sig.EsVacia())
		{
			this -> Insertar(l.Obtener(sig));
			sig = sig.Siguiente();
		}
	}
	
	return *this;
}
	
bool TListaCalendario::operator==(const TListaCalendario &l)
{
	TListaPos my_sig, other_sig;
	my_sig = this -> Primera();
	other_sig = l.Primera();
	bool same = true;
	
	if(this -> Longitud() == l.Longitud())
	{
		while(!my_sig.EsVacia())
		{
			if(my_sig.pos -> c != other_sig.pos -> c)
				same = false;
			
			my_sig = my_sig.Siguiente();
			other_sig = other_sig.Siguiente();
		}
		
		return same;
	}
	else
		return false;
}

TListaCalendario TListaCalendario::operator+(const TListaCalendario &l)
{
	if(l.EsVacia())
		return *this;
	else if(EsVacia())
		return l;
	else
	{		
		TListaCalendario n_lista(*this);
		TListaPos pos;
		
		pos = l.Primera();
		
		while(!pos.EsVacia())
		{
			n_lista.Insertar(l.Obtener(pos));
			pos = pos.Siguiente();
		}
		
		return n_lista;
	}
}

TListaCalendario TListaCalendario::operator-(const TListaCalendario &l)
{
	TListaCalendario new_list;
	TListaPos my_sig;
	TCalendario for_insert;
	
	my_sig = this -> Primera();
	
	while(!my_sig.EsVacia())
	{
		for_insert = my_sig.pos -> c;
		
		if(!l.Buscar(for_insert))
			new_list.Insertar(for_insert);
		
		my_sig = my_sig.Siguiente();
	}
	
	return new_list;
}

bool TListaCalendario::Insertar(const TCalendario &cal)
{	
	TListaPos sig, ant;
	
	if(!this -> Buscar(cal))
	{
		if(primero != NULL)
		{
			TNodoCalendario *nuevo;
			nuevo = new TNodoCalendario();
			nuevo -> c = cal;
			
			sig = Primera();
			bool salir = false;
			
			while(!salir && !sig.EsVacia())
			{
				if(Obtener(sig) > cal)
				{
					salir = true;
				}
				else
				{
					ant = sig;
					sig = sig.Siguiente();
				}
			}
			if(sig == Primera())
			{
				nuevo -> siguiente = sig.pos;
				primero = nuevo;
			}
			else if(sig != Ultima())
			{
				nuevo -> siguiente = sig.pos;
				ant.pos -> siguiente = nuevo;
			}
			else
			{
				if(!salir)
					sig.pos -> siguiente = nuevo;
				else
				{
					nuevo -> siguiente = sig.pos;
					ant.pos -> siguiente = nuevo;
				}
			}
			
			return true;			
		}
		else
		{
			primero = new TNodoCalendario();
			primero -> c = cal;
			
			return true;
		}
	}
	else
		return false;
}

bool TListaCalendario::Borrar(const TCalendario &cal)
{
	TListaPos sig, ant, a_ant;
	bool salir = false;
	
	if(Buscar(cal))
	{
		if(primero != NULL)
		{
			ant = Primera();
			sig = ant.Siguiente();
		
			while(!salir && !ant.EsVacia())
			{
				if(ant.pos -> c == cal)
				{
					salir = true;
				}
				else
				{
					a_ant = ant;
					ant = sig;
					sig = sig.Siguiente();
				}
			}
		
			if(salir)
			{
				if(ant == Primera())
				{
					primero = sig.pos;
					delete ant.pos;
				}
				else if(ant != Ultima())
				{
					a_ant.pos -> siguiente = sig.pos;
					delete ant.pos;
				}
				else
				{
					a_ant.pos -> siguiente = NULL;
					delete ant.pos;
				}
				
				return true;
			}
		}
	}
	
	return false;
}

bool TListaCalendario::Borrar(const TListaPos &pos)
{	
	return Borrar(Obtener(pos));
}

bool TListaCalendario::Borrar(const int d, const int m, const int a)
{
	TListaPos sig, ant;
	TCalendario cal(d, m, a, (char *) "");
	bool borra = false;
	
	if(!cal.EsVacio())
	{
		cal.ModMensaje(NULL);
		
		sig = this -> Primera();
		
		while(sig.pos != NULL)
		{
			ant = sig;
			sig.pos = sig.pos -> siguiente;
			
			if(ant.pos -> c < cal)
			{
				Borrar(ant);
				borra = true;
			}
		}
	}
	
	return borra;
}

bool TListaCalendario::EsVacia() const
{
	return primero == NULL;
}

TCalendario TListaCalendario::Obtener(const TListaPos &p) const
{
	return p.pos -> c;
}

bool TListaCalendario::Buscar(const TCalendario &cal) const
{
	TListaPos sig;
	sig = this -> Primera();
	
	while(sig.pos != NULL)
	{
		if(sig.pos -> c == cal)
			return true;
			
		sig.pos = sig.pos -> siguiente;
	}
	
	return false;
}

int TListaCalendario::Longitud() const
{
	int tam = 0;
	TListaPos sig;
	sig.pos = primero;
	
	while(sig.pos != NULL)
	{
		tam++;
		sig.pos = sig.pos -> siguiente;
	}
	return tam;
}

TListaPos TListaCalendario::Primera() const 
{
	TListaPos p;
	
	if(!EsVacia())
	{
		p.pos = primero;
	}
	
	return p;
}

TListaPos TListaCalendario::Ultima() const
{
	TListaPos ant, sig;
	sig = Primera();
	
	if(!EsVacia())
	{
		while(!sig.EsVacia())
		{	
			ant = sig;
			sig = sig.Siguiente();
		}
	}
		
	return ant;
}

TListaCalendario TListaCalendario::SumarSubl(const int i_l1, const int f_l1, const TListaCalendario &l2, const int i_l2, const int f_l2)
{
	TListaCalendario l1_copy(*this), l2_copy(l2);
	
		return l1_copy.ExtraerRango(i_l1, f_l1) + l2_copy.ExtraerRango(i_l2, f_l2);
}

TListaCalendario TListaCalendario::ExtraerRango(int n1, int n2)
{
	TListaCalendario n_lista;
	TListaPos ant;
	TListaPos pos;
		
	if(!(*this).EsVacia())
	{
		pos = Primera();
		
		if(n1 <= n2)
		{
			if(n1 <= 0)
				n1 = 1;
			
			for(int i = 1; i < n1; i++)
			{
				ant = pos;
				pos = pos.Siguiente();
			}
			
			if(n1 < n2)
			{
				if(n2 > Longitud())
					n2 = Longitud();
					
				for(int i = n1; i <= n2; i++)
				{
					n_lista.Insertar(Obtener(pos));
					ant = pos;
					pos = pos.Siguiente();
					Borrar(ant);
				}
			}
			else if(n1 == n2)
			{
				n_lista.Insertar(Obtener(pos));
				Borrar(pos);
			}
		}
	}
	
	return n_lista;
}

TListaPos::TListaPos()
{
	pos = NULL;
}

TListaPos::TListaPos(TListaPos &posi)
{
	if(pos != NULL)
		delete pos;
		
	pos = posi.pos;
}

TListaPos::~TListaPos()
{
	pos = NULL;
}

TListaPos& TListaPos::operator=(const TListaPos &posi)
{		
	pos = posi.pos;
	
	return *this;
}

bool TListaPos::operator==(const TListaPos &posi)
{
	return pos == posi.pos;
}

bool TListaPos::operator!=(const TListaPos& pos)
{
	return !(*this == pos);
}

TListaPos TListaPos::Siguiente()
{
	TListaPos p;
	
	p.pos = pos -> siguiente;
	
	return p;
}

bool TListaPos::EsVacia()
{
	return pos == NULL;
}

TNodoCalendario::TNodoCalendario():c()
{
	siguiente = NULL;
}

TNodoCalendario::TNodoCalendario(TNodoCalendario &nodo):c(nodo.c)
{	
	siguiente = NULL;
}

TNodoCalendario::~TNodoCalendario()
{
	siguiente = NULL;
}

TNodoCalendario& TNodoCalendario::operator=(TNodoCalendario &nodo)
{		
	c = nodo.c;
	siguiente = NULL;
	
	return *this;
}
