//PRUEBAS CRISTINA RIVERA BAYDAL
#include <iostream>
#include "tvectorcalendario.h"
#include "tlistacalendario.h"
#include "tavlcalendario.h"

using namespace std;

int main(){

	cout <<  "********************************************" << endl;
	cout <<  "**           Pruebas Árboles AVL          **" << endl;
	cout <<  "**       Ejercicio transparencia 16       **" << endl;
	cout <<  "********************************************" << endl;

	TCalendario p05(1, 1, 2005, (char*)"p05");
	TCalendario p10(2, 1, 2010, (char*)"p10");
	TCalendario p20(1, 3, 2020, (char*)"p20");
	TCalendario p25(4, 1, 2025, (char*)"p25");
	TCalendario p30(5, 1, 2030, (char*)"p30");
	TCalendario p32(1, 1, 2032, (char*)"p32");
	TCalendario p35(3, 1, 2035, (char*)"p35");
	TCalendario p40(5, 1, 2040, (char*)"p40");
	TCalendario p42(4, 1, 2042, (char*)"p42");
	TCalendario p45(1, 1, 2045, (char*)"p45");
	TCalendario p50(3, 1, 2050, (char*)"p50");
	TCalendario p55(5, 1, 2055, (char*)"p55");

	TAVLCalendario pr1;
	pr1.Insertar(p40);
	pr1.Insertar(p30);
	pr1.Insertar(p50);
	pr1.Insertar(p20);
	pr1.Insertar(p35);
	pr1.Insertar(p45);
	pr1.Insertar(p55);
	pr1.Insertar(p10);
	pr1.Insertar(p25);
	pr1.Insertar(p32);
	pr1.Insertar(p42);
	pr1.Insertar(p05);

	cout << "Preorden  " << endl << pr1.Preorden() << endl;
	cout << "Postorden " << endl << pr1.Postorden() << endl;
	cout << "Inorden   " << endl << pr1.Inorden() << endl;

	cout << endl <<  "****************** Prueba1 *****************" << endl;//***********************************************************
	cout << endl;//***********************************************************************************************************************
	pr1.Borrar(p55);

	TVectorCalendario r1 = pr1.Preorden();
	TVectorCalendario r2 = pr1.Postorden();
	TVectorCalendario r3 = pr1.Inorden();

	if (pr1.Nodos()==11){
			if (r1[1]==p30 &&
				r1[2]==p20 &&
				r1[3]==p10 &&
				r1[4]==p05 &&
				r1[5]==p25 &&
				r1[6]==p40 &&
				r1[7]==p35 &&
				r1[8]==p32 &&
				r1[9]==p45 &&
				r1[10]==p42 &&
				r1[11]==p50)
			{
				cout << "      Prueba 1: Preorden ---- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Preorden no es la correcta"<<endl;
				cout << pr1.Preorden() << endl;
			}
			if (r2[1]==p05 &&
				r2[2]==p10 &&
				r2[3]==p25 &&
				r2[4]==p20 &&
				r2[5]==p32 &&
				r2[6]==p35 &&
				r2[7]==p42 &&
				r2[8]==p50 &&
				r2[9]==p45 &&
				r2[10]==p40 &&
				r2[11]==p30)
			{
				cout << "      Prueba 1: Postorden --- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Postorden no es la correcta"<<endl;
				cout << pr1.Postorden() << endl;
			}
			if (r3[1]==p05 &&
				r3[2]==p10 &&
				r3[3]==p20 &&
				r3[4]==p25 &&
				r3[5]==p30 &&
				r3[6]==p32 &&
				r3[7]==p35 &&
				r3[8]==p40 &&
				r3[9]==p42 &&
				r3[10]==p45 &&
				r3[11]==p50)
			{
				cout << "      Prueba 1: Inorden ----- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Inorden no es la correcta"<<endl;
				cout << pr1.Inorden() << endl;
			}
	    }
	    else
	    {
	        if (pr1.Nodos()<10)
	        	cout << "      ERROR: Prueba 1: Se han borrado de más"<<endl;
	        else
	        	cout << "      ERROR: Prueba 1: No se han borrado"<<endl;
	    }

	cout << endl <<  "****************** Prueba2 *****************" << endl;//***********************************************************
	cout << endl;//***********************************************************************************************************************
	pr1.Borrar(p32);

	r1 = pr1.Preorden();
	r2 = pr1.Postorden();
	r3 = pr1.Inorden();


	if (pr1.Nodos()==10){
			if (r1[1]==p30 &&
				r1[2]==p20 &&
				r1[3]==p10 &&
				r1[4]==p05 &&
				r1[5]==p25 &&
				r1[6]==p40 &&
				r1[7]==p35 &&
				r1[8]==p45 &&
				r1[9]==p42 &&
				r1[10]==p50)
			{
				cout << "      Prueba 1: Preorden ---- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Preorden no es la correcta"<<endl;
				cout << pr1.Preorden() << endl;
			}
			if (r2[1]==p05 &&
				r2[2]==p10 &&
				r2[3]==p25 &&
				r2[4]==p20 &&
				r2[5]==p35 &&
				r2[6]==p42 &&
				r2[7]==p50 &&
				r2[8]==p45 &&
				r2[9]==p40 &&
				r2[10]==p30)
			{
				cout << "      Prueba 1: Postorden --- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Postorden no es la correcta"<<endl;
				cout << pr1.Postorden() << endl;
			}
			if (r3[1]==p05 &&
				r3[2]==p10 &&
				r3[3]==p20 &&
				r3[4]==p25 &&
				r3[5]==p30 &&
				r3[6]==p35 &&
				r3[7]==p40 &&
				r3[8]==p42 &&
				r3[9]==p45 &&
				r3[10]==p50)
			{
				cout << "      Prueba 1: Inorden ----- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Inorden no es la correcta"<<endl;
				cout << pr1.Inorden() << endl;
			}
	    }
	    else
	    {
	        if (pr1.Nodos()<9)
	        	cout << "      ERROR: Prueba 1: Se han borrado de más"<<endl;
			else
				cout << "      ERROR: Prueba 1: No se han borrado"<<endl;
		}

	cout << endl <<  "****************** Prueba3 *****************" << endl;//***********************************************************
	cout << endl;//***********************************************************************************************************************

	pr1.Borrar(p40);

	r1 = pr1.Preorden();
	r2 = pr1.Postorden();
	r3 = pr1.Inorden();

	if (pr1.Nodos()==9){
			if (r1[1]==p30 &&
				r1[2]==p20 &&
				r1[3]==p10 &&
				r1[4]==p05 &&
				r1[5]==p25 &&
				r1[6]==p45 &&
				r1[7]==p35 &&
				r1[8]==p42 &&
				r1[9]==p50)
			{
				cout << "      Prueba 1: Preorden ---- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Preorden no es la correcta"<<endl;
				cout << pr1.Preorden() << endl;
			}
			if (r2[1]==p05 &&
				r2[2]==p10 &&
				r2[3]==p25 &&
				r2[4]==p20 &&
				r2[5]==p42 &&
				r2[6]==p35 &&
				r2[7]==p50 &&
				r2[8]==p45 &&
				r2[9]==p30)
			{
				cout << "      Prueba 1: Postorden --- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Postorden no es la correcta"<<endl;
				cout << pr1.Postorden() << endl;
			}
			if (r3[1]==p05 &&
				r3[2]==p10 &&
				r3[3]==p20 &&
				r3[4]==p25 &&
				r3[5]==p30 &&
				r3[6]==p35 &&
				r3[7]==p42 &&
				r3[8]==p45 &&
				r3[9]==p50)
			{
				cout << "      Prueba 1: Inorden ----- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Inorden no es la correcta"<<endl;
				cout << pr1.Inorden() << endl;
			}
	    }
	    else
	    {
	        if (pr1.Nodos()<8)
	        	cout << "      ERROR: Prueba 1: Se han borrado de más"<<endl;
			else
				cout << "      ERROR: Prueba 1: No se han borrado"<<endl;
	    }


	cout << endl <<  "****************** Prueba4 *****************" << endl;//***********************************************************
	cout << endl;//***********************************************************************************************************************

	pr1.Borrar(p30);

	r1 = pr1.Preorden();
	r2 = pr1.Postorden();
	r3 = pr1.Inorden();

	if (pr1.Nodos()==8){
		if (r1[1]==p25 &&
			r1[2]==p10 &&
			r1[3]==p05 &&
			r1[4]==p20 &&
			r1[5]==p45 &&
			r1[6]==p35 &&
			r1[7]==p42 &&
			r1[8]==p50)
		{
			cout << "      Prueba 1: Preorden ---- ok"<<endl;
		}
		else
		{
			cout << "      ERROR: La estructura en Preorden no es la correcta"<<endl;
			cout << pr1.Preorden() << endl;
		}
		if (r2[1]==p05 &&
			r2[2]==p20 &&
			r2[3]==p10 &&
			r2[4]==p42 &&
			r2[5]==p35 &&
			r2[6]==p50 &&
			r2[7]==p45 &&
			r2[8]==p25)
		{
			cout << "      Prueba 1: Postorden --- ok"<<endl;
		}
		else
		{
			cout << "      ERROR: La estructura en Postorden no es la correcta"<<endl;
			cout << pr1.Postorden() << endl;
		}
		if (r3[1]==p05 &&
			r3[2]==p10 &&
			r3[3]==p20 &&
			r3[4]==p25 &&
			r3[5]==p35 &&
			r3[6]==p42 &&
			r3[7]==p45 &&
			r3[8]==p50)
		{
			cout << "      Prueba 1: Inorden ----- ok"<<endl;
		}
		else
		{
			cout << "      ERROR: La estructura en Inorden no es la correcta"<<endl;
			cout << pr1.Inorden() << endl;
		}
	}
	else
	{
		if (pr1.Nodos()<4)
			cout << "      ERROR: Prueba 1: Se han borrado de más"<<endl;
		else
			cout << "      ERROR: Prueba 1: No se han borrado"<<endl;
	}

	cout << endl <<  "**************** FIN DEL TAD ***************" << endl;
}
