//PRUEBAS CRISTINA RIVERA BAYDAL
#include <iostream>
#include "tvectorcalendario.h"
#include "tlistacalendario.h"
#include "tavlcalendario.h"

using namespace std;

int main(){

	cout <<  "********************************************" << endl;
	cout <<  "**                 Pruebas                **" << endl;
	cout <<  "********************************************" << endl;

	TCalendario p01(1, 1, 2001, (char*)"p01");
	TCalendario p02(2, 1, 2002, (char*)"p02");
	TCalendario p03(1, 3, 2003, (char*)"p03");
	TCalendario p04(4, 1, 2004, (char*)"p04");
	TCalendario p05(5, 1, 2005, (char*)"p05");
	TCalendario p06(1, 1, 2006, (char*)"p06");
	TCalendario p07(3, 1, 2007, (char*)"p07");
	TCalendario p08(5, 1, 2008, (char*)"p08");
	TCalendario p09(4, 1, 2009, (char*)"p09");
	TCalendario p10(1, 1, 2010, (char*)"p10");
	TCalendario p11(3, 1, 2011, (char*)"p11");
	TCalendario p12(5, 1, 2012, (char*)"p12");
	TCalendario p13(4, 1, 2013, (char*)"p13");
	TCalendario p14(5, 1, 2014, (char*)"p14");
	TCalendario p15(4, 1, 2015, (char*)"p15");

	TAVLCalendario pr1;
	pr1.Insertar(p05);
	pr1.Insertar(p03);
	pr1.Insertar(p08);
	pr1.Insertar(p02);
	pr1.Insertar(p04);
	pr1.Insertar(p07);
	pr1.Insertar(p10);
	pr1.Insertar(p01);
	pr1.Insertar(p06);
	pr1.Insertar(p09);
	pr1.Insertar(p11);
//	cout << "Preorden  " << endl << pr1.Preorden() << endl;
//	cout << "Postorden " << endl << pr1.Postorden() << endl;
//	cout << "Inorden   " << endl << pr1.Inorden() << endl;

	cout << endl <<  "****************** Prueba1 ******************" << endl;//***********************************************************
	cout << endl;//***********************************************************************************************************************
	pr1.Borrar(p04);

	TVectorCalendario r1 = pr1.Preorden();
	TVectorCalendario r2 = pr1.Postorden();
	TVectorCalendario r3 = pr1.Inorden();

	if (pr1.Nodos()==10){
			if (r1[1]==p05 &&
				r1[2]==p02 &&
				r1[3]==p01 &&
				r1[4]==p03 &&
				r1[5]==p08 &&
				r1[6]==p07 &&
				r1[7]==p06 &&
				r1[8]==p10 &&
				r1[9]==p09 &&
				r1[10]==p11)
			{
				cout << "      Prueba 1: Preorden ---- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Preorden no es la correcta"<<endl;
				cout << pr1.Preorden() << endl;
			}
			if (r2[1]==p01 &&
				r2[2]==p03 &&
				r2[3]==p02 &&
				r2[4]==p06 &&
				r2[5]==p07 &&
				r2[6]==p09 &&
				r2[7]==p11 &&
				r2[8]==p10 &&
				r2[9]==p08 &&
				r2[10]==p05)
			{
				cout << "      Prueba 1: Postorden --- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Postorden no es la correcta"<<endl;
				cout << pr1.Postorden() << endl;
			}
			if (r3[1]==p01 &&
				r3[2]==p02 &&
				r3[3]==p03 &&
				r3[4]==p05 &&
				r3[5]==p06 &&
				r3[6]==p07 &&
				r3[7]==p08 &&
				r3[8]==p09 &&
				r3[9]==p10 &&
				r3[10]==p11)
			{
				cout << "      Prueba 1: Inorden ----- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Inorden no es la correcta"<<endl;
				cout << pr1.Inorden() << endl;
			}
	    }
	    else
	    {
	        if (pr1.Nodos()<10)
	        	cout << "      ERROR: Prueba 1: Se han borrado de más"<<endl;
	        else
	        	cout << "      ERROR: Prueba 1: No se han borrado"<<endl;
	    }

	cout << endl <<  "****************** Prueba2 ******************" << endl;//***********************************************************
	cout << endl;//***********************************************************************************************************************
	pr1.Borrar(p08);

	r1 = pr1.Preorden();
	r2 = pr1.Postorden();
	r3 = pr1.Inorden();

	if (pr1.Nodos()==9){
			if (r1[1]==p05 &&
				r1[2]==p02 &&
				r1[3]==p01 &&
				r1[4]==p03 &&
				r1[5]==p07 &&
				r1[6]==p06 &&
				r1[7]==p10 &&
				r1[8]==p09 &&
				r1[9]==p11)
			{
				cout << "      Prueba 1: Preorden ---- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Preorden no es la correcta"<<endl;
				cout << pr1.Preorden() << endl;
			}
			if (r2[1]==p01 &&
				r2[2]==p03 &&
				r2[3]==p02 &&
				r2[4]==p06 &&
				r2[5]==p09 &&
				r2[6]==p11 &&
				r2[7]==p10 &&
				r2[8]==p07 &&
				r2[9]==p05)
			{
				cout << "      Prueba 1: Postorden --- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Postorden no es la correcta"<<endl;
				cout << pr1.Postorden() << endl;
			}
			if (r3[1]==p01 &&
				r3[2]==p02 &&
				r3[3]==p03 &&
				r3[4]==p05 &&
				r3[5]==p06 &&
				r3[6]==p07 &&
				r3[7]==p09 &&
				r3[8]==p10 &&
				r3[9]==p11)
			{
				cout << "      Prueba 1: Inorden ----- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Inorden no es la correcta"<<endl;
				cout << pr1.Inorden() << endl;
			}
	    }
	    else
	    {
	        if (pr1.Nodos()<9)
	        	cout << "      ERROR: Prueba 1: Se han borrado de más"<<endl;
			else
				cout << "      ERROR: Prueba 1: No se han borrado"<<endl;
		}

	cout << endl <<  "****************** Prueba3 ******************" << endl;//***********************************************************
	cout << endl;//***********************************************************************************************************************

	pr1.Borrar(p06);

	r1 = pr1.Preorden();
	r2 = pr1.Postorden();
	r3 = pr1.Inorden();

	if (pr1.Nodos()==8){
			if (r1[1]==p05 &&
				r1[2]==p02 &&
				r1[3]==p01 &&
				r1[4]==p03 &&
				r1[5]==p10 &&
				r1[6]==p07 &&
				r1[7]==p09 &&
				r1[8]==p11)
			{
				cout << "      Prueba 1: Preorden ---- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Preorden no es la correcta"<<endl;
				cout << pr1.Preorden() << endl;
			}
			if (r2[1]==p01 &&
				r2[2]==p03 &&
				r2[3]==p02 &&
				r2[4]==p09 &&
				r2[5]==p07 &&
				r2[6]==p11 &&
				r2[7]==p10 &&
				r2[8]==p05)
			{
				cout << "      Prueba 1: Postorden --- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Postorden no es la correcta"<<endl;
				cout << pr1.Postorden() << endl;
			}
			if (r3[1]==p01 &&
				r3[2]==p02 &&
				r3[3]==p03 &&
				r3[4]==p05 &&
				r3[5]==p07 &&
				r3[6]==p09 &&
				r3[7]==p10 &&
				r3[8]==p11)
			{
				cout << "      Prueba 1: Inorden ----- ok"<<endl;
			}
			else
			{
				cout << "      ERROR: La estructura en Inorden no es la correcta"<<endl;
				cout << pr1.Inorden() << endl;
			}
	    }
	    else
	    {
	        if (pr1.Nodos()<8)
	        	cout << "      ERROR: Prueba 1: Se han borrado de más"<<endl;
			else
				cout << "      ERROR: Prueba 1: No se han borrado"<<endl;
	    }


	cout << endl <<  "****************** Prueba4 ******************" << endl;//***********************************************************
	cout << endl;//***********************************************************************************************************************

	pr1.Borrar(p05);
	pr1.Borrar(p02);
	pr1.Borrar(p01);
	pr1.Borrar(p07);

	r1 = pr1.Preorden();
	r2 = pr1.Postorden();
	r3 = pr1.Inorden();

	if (pr1.Nodos()==4){
		if (r1[1]==p10 &&
			r1[2]==p03 &&
			r1[3]==p09 &&
			r1[4]==p11)
		{
			cout << "      Prueba 1: Preorden ---- ok"<<endl;
		}
		else
		{
			cout << "      ERROR: La estructura en Preorden no es la correcta"<<endl;
			cout << pr1.Preorden() << endl;
		}
		if (r2[1]==p09 &&
			r2[2]==p03 &&
			r2[3]==p11 &&
			r2[4]==p10)
		{
			cout << "      Prueba 1: Postorden --- ok"<<endl;
		}
		else
		{
			cout << "      ERROR: La estructura en Postorden no es la correcta"<<endl;
			cout << pr1.Postorden() << endl;
		}
		if (r3[1]==p03 &&
			r3[2]==p09 &&
			r3[3]==p10 &&
			r3[4]==p11)
		{
			cout << "      Prueba 1: Inorden ----- ok"<<endl;
		}
		else
		{
			cout << "      ERROR: La estructura en Inorden no es la correcta"<<endl;
			cout << pr1.Inorden() << endl;
		}
	}
	else
	{
		if (pr1.Nodos()<4)
			cout << "      ERROR: Prueba 1: Se han borrado de más"<<endl;
		else
			cout << "      ERROR: Prueba 1: No se han borrado"<<endl;
	}
	cout << endl <<  "************** FIN DEL TAD **************" << endl;
}
