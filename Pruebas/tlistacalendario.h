#ifndef __TLISTACALENDARIO_H_
#define __TLISTACALENDARIO_H_

#include <iostream>

using namespace std;

#include "tcalendario.h"

class TNodoCalendario
{
	friend class TListaCalendario;
	friend class TListaPos;
	
	private:
		TCalendario c;
		TNodoCalendario *siguiente;
		
	public:
		TNodoCalendario();
		TNodoCalendario(TNodoCalendario&);
		~TNodoCalendario();
		TNodoCalendario &operator=(TNodoCalendario&);
};

class TListaPos
{
	friend class TListaCalendario;
	
	private:
		TNodoCalendario *pos;
		
	public:
		TListaPos();
		TListaPos(TListaPos&);
		~TListaPos();
		TListaPos &operator=(const TListaPos&);
		
		bool operator==(const TListaPos&);
		bool operator!=(const TListaPos&);
		TListaPos Siguiente();
		bool EsVacia();
};

class TListaCalendario
{
	friend ostream& operator<<(ostream&, const TListaCalendario&);
	
	private:
		TNodoCalendario *primero;
	
	public:
		TListaCalendario();
		TListaCalendario(const TListaCalendario&);
		~TListaCalendario();
		TListaCalendario &operator=(const TListaCalendario&);
		
		bool operator==(const TListaCalendario&);
		TListaCalendario operator+(const TListaCalendario&);
		TListaCalendario operator-(const TListaCalendario&);
		bool Insertar(const TCalendario&);
		bool Borrar(const TCalendario&);
		bool Borrar(const TListaPos&);
		bool Borrar(const int, const int, const int);
		bool EsVacia() const;
		TCalendario Obtener(const TListaPos&) const;
		bool Buscar(const TCalendario&) const;
		int Longitud() const;
		TListaPos Primera() const;
		TListaPos Ultima() const ;
		TListaCalendario SumarSubl(const int , const int, const TListaCalendario&, const int, const int);
		TListaCalendario ExtraerRango(int, int);
};

#endif
