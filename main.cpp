#include <iostream>
#include <cstring>

using namespace std;

#include "tcalendario.h"
#include "tvectorcalendario.h"
#include "tlistacalendario.h"
#include "tabbcalendario.h"
#include "tavlcalendario.h"


int main(int argc, char **argv)
{	
	TAVLCalendario arbol;
	TNodoAVL nodo;

	cout << endl << "\tConstructores por defecto: correcto." << endl;

	TCalendario c1(1, 1, 1900, (char *) "1");
	TCalendario c2(2, 1, 1900, (char *) "2");
	TCalendario c3(3, 1, 1900, (char *) "3");
	TCalendario c4(4, 1, 1900, (char *) "4");
	TCalendario c5(5, 1, 1900, (char *) "5");
	TCalendario c6(6, 1, 1900, (char *) "6");
	TCalendario c7(7, 1, 1900, (char *) "7");
	TCalendario c8(8, 1, 1900, (char *) "8");
	TCalendario c9(9, 1, 1900, (char *) "9");
	TCalendario c10(10, 1, 1900, (char *) "10");
	TCalendario c11(11, 1, 1900, (char *) "11");
	TCalendario c12(12, 1, 1900, (char *) "12");
	TCalendario c13(13, 1, 1900, (char *) "13");

	if(arbol.Insertar(c5) && arbol.Insertar(c1) && arbol.Insertar(c10))
		cout << "\tInsertar tres nodos diferentes (5, 1, 10): correcto." << endl;
	else
		cout << "\tError: Insertar (5, 1, 10)." << endl;

	if(!arbol.Insertar(c10) && !arbol.Insertar(c5))
		cout << "\tNo inserta dos nodos repetidos (10, 5): correcto." << endl;
	else
		cout << "\tError: Insertar (10, 5)" << endl;

	if(arbol.Nodos() == 3)
		cout << "\tComprobar numero de nodos del arbol (3): correcto." << endl;
	else
		cout << "\tError: Nodos es incorrecto o algo ha fallado en las pruebas anteriores." << endl;

	if(arbol.NodosHoja() == 2)
		cout << "\tComprobar numero de nodos hoja (2): correcto." << endl;
	else
		cout << "\tError: NodosHoja." << endl;

	TAVLCalendario arbol2(arbol);

	if(arbol == arbol2)
		cout << "\tConstructor de copia: correcto." << endl;
	else
		cout << "\tError: se puede encontrar en el constructor de copia o en el operator==." << endl;

	TVectorCalendario v(3);
	v[1] = c1;
	v[2] = c5;
	v[3] = c10;

	if(arbol.Inorden() == v)
		cout << "\tInorden: correcto." << endl;
	else
		cout << "\tError: Inorden." << endl;

	v[1] = c5;
	v[2] = c1;
	v[3] = c10;

	if(arbol.Preorden() == v)
		cout << "\tPreorden: correcto." << endl;
	else
		cout << "\tError: Preorden." << endl;

	v[1] = c1;
	v[2] = c10;
	v[3] = c5;

	if(arbol.Postorden() == v)
		cout << "\tPostorden: correcto." << endl;
	else
		cout << "\tError: Postorden." << endl;

	cout << endl << "\tPruebas de Insertar." << endl;

	arbol.Insertar(c4);

	v.Redimensionar(4);

	v[1] = c1;
	v[2] = c4;
	v[3] = c5;
	v[4] = c10;

	if(arbol.Inorden() == v && arbol.Nodos() == 4)
		cout << "\tInsertado un nodo que no requiere rotacion (4): correcto." << endl;
	else
		cout << "\tError: Insertar (4)." << endl;

	arbol.Insertar(c3);

	v.Redimensionar(5);

	v[1] = c1;	
	v[2] = c3;
	v[3] = c4;
	v[4] = c5;
	v[5] = c10;

	if(arbol.Inorden() == v && arbol.Nodos() == 5)
		cout << "\tInsertado nodo que requiere rotacion DI (3): correcto." << endl;
	else
		cout << "\tError: Insertar (3)." << endl;

	arbol.Insertar(c7);

	v.Redimensionar(6);	

	v[1] = c1;	
	v[2] = c3;
	v[3] = c4;
	v[4] = c5;
	v[5] = c7;
	v[6] = c10;

	if(arbol.Inorden() == v && arbol.Nodos() == 6)
		cout << "\tInsertado un nodo que no requiere rotacion (7): correcto." << endl;
	else
		cout << "\tError: Insertar(7)." << endl;

	arbol.Insertar(c6);

	v.Redimensionar(7);	

	v[1] = c1;	
	v[2] = c3;
	v[3] = c4;
	v[4] = c5;
	v[5] = c6;
	v[6] = c7;
	v[7] = c10;


	if(arbol.Inorden() == v && arbol.Nodos() == 7)
		cout << "\tInsertar nodo que requiere rotacion II (6): correcto." << endl;
	else
		cout << "\tError: Insertar (6)." << endl;

	arbol.Insertar(c12);
	arbol.Insertar(c13);

	v.Redimensionar(9);	

	v[1] = c1;	
	v[2] = c3;
	v[3] = c4;
	v[4] = c5;
	v[5] = c6;
	v[6] = c7;
	v[7] = c10;
	v[8] = c12;
	v[9] = c13;

	if(arbol.Inorden() == v && arbol.Nodos() == 9)
		cout << "\tInsertar dos nodos, sin rotacion (12), con rotacion DD (13): correcto." << endl;
	else
		cout << "\tError: Insertar(12, 13)." << endl;

	arbol.Insertar(c11);

	v.Redimensionar(10);	

	v[1] = c1;	
	v[2] = c3;
	v[3] = c4;
	v[4] = c5;
	v[5] = c6;
	v[6] = c7;
	v[7] = c10;
	v[8] = c11;
	v[9] = c12;
	v[10] = c13;

	if(arbol.Inorden() == v)
		cout << "\tInsertar un nodo con rotacion DI (11): correcto." << endl;
	else
		cout << "\tError: Insertar(11)." << endl;


	cout << endl << "\tDesde un nuevo arbol probamos la rotacion ID." << endl;

	TAVLCalendario arbol3;

	arbol3.Insertar(c5);
	arbol3.Insertar(c3);
	arbol3.Insertar(c4);

	v.Redimensionar(3);
	v[1] = c3;
	v[2] = c4;
	v[3] = c5;

	if(arbol3.Inorden() == v && arbol3.Nodos() == 3)
		cout << "\tInsertar nodo con rotacion ID (3): correcto." << endl;
	else
		cout << "\tError: Insertar (3)." << endl;

	cout << endl << "\tPruebas de Borrar." << endl;

	v.Redimensionar(9);

	v[1] = c1;	
	v[2] = c3;
	v[3] = c4;
	v[4] = c6;
	v[5] = c7;
	v[6] = c10;
	v[7] = c11;
	v[8] = c12;
	v[9] = c13;

	if(arbol.Borrar(c5))
	{
		if(arbol.Inorden() == v)
			cout << "\tBorrar la raiz (5) sin rotacion: correcto." << endl;
		else
			cout << "\tError: Borrar (5), rotacion incorrecta." << endl;
	}
	else
		cout << "\tError: Borrar (5), ha sido falso." << endl;

	v.Redimensionar(8);

	v[1] = c1;	
	v[2] = c3;
	v[3] = c6;
	v[4] = c7;
	v[5] = c10;
	v[6] = c11;
	v[7] = c12;
	v[8] = c13;

	if(arbol.Borrar(c4))
	{
		if(arbol.Inorden() == v && arbol.Nodos() == 8)
			cout << "\tBorrar la raiz (4) con rotacion DD (-2, 0): correcto." << endl;
		else
			cout << "\tError: Borrar (4), rotacion incorrecta." << endl;
	}
	else
		cout << "\tError: Borrar (4), ha sido falso." << endl;


	v.Redimensionar(7);

	v[1] = c3;	
	v[2] = c6;
	v[3] = c7;
	v[4] = c10;
	v[5] = c11;
	v[6] = c12;
	v[7] = c13;

	if(arbol.Borrar(c1))
	{
		if(arbol.Inorden() == v && arbol.Nodos() == 7)
			cout << "\tBorrar un nodo (1) con rotacion DI: correcto." << endl;
		else
			cout << "\tError: Borrar (1), rotacion incorrecta." << endl;
	}
	else
		cout << "\tError: Borrar (1), ha sido falso." << endl;

	v.Redimensionar(5);

	v[1] = c3;	
	v[2] = c6;
	v[3] = c7;
	v[4] = c10;
	v[5] = c12;

	if(arbol.Borrar(c11) && arbol.Borrar(c13))
	{
		if(arbol.Inorden() == v && arbol.Nodos() == 5)
			cout << "\tBorrar dos nodos (11, 13) sin rotacion: correcto." << endl;
		else
			cout << "\tError: Borrar (11, 13)." << endl;
	}
	else
		cout << "\tError: Borrar (11, 13), ha sido falso." << endl;

	v.Redimensionar(4);

	v[1] = c3;	
	v[2] = c6;
	v[3] = c7;
	v[4] = c10;

	if(arbol.Borrar(c12))
	{
		if(arbol.Inorden() == v && arbol.Nodos() == 4)
			cout << "\tBorrar dos nodos (12) con rotacion II (+2, 0): correcto." << endl;
		else
			cout << "\tError: Borrar (12)." << endl;
	}
	else
		cout << "\tError: Borrar (12), ha sido falso." << endl;


	TAVLCalendario arbol4;

	arbol4.Insertar(c5);
	arbol4.Insertar(c3);
	arbol4.Insertar(c7);
	arbol4.Insertar(c4);

	v.Redimensionar(3);
	v[1] = c3;
	v[2] = c4;
	v[3] = c5;	

	cout << endl << "\tPruebo la rotacion ID en borrar con un arbol nuevo." << endl;

	if(arbol4.Borrar(c7))
	{
		if(arbol4.Inorden() == v && arbol4.Nodos() == 3)
			cout << "\tBorrar un nodo (7) con rotacion ID: correcto." << endl << endl;
		else
			cout << "\tError: Borrar (7)." << endl << endl;
	}
	else
		cout << "\tError: Borrar (7), ha sido falso." << endl << endl;


	if(!arbol4.Borrar(c13))
	{
		cout << "\tIntentar borrar un nodo que no existe: correcto" << endl;
	}
	else
		cout << "\tError: Borrar (13) ha sido verdadero." << endl;

	cout << endl;

	return 0;
}