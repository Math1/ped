#ifndef __TCALENDARIO_H_
#define __TCALENDARIO_H_

#include <iostream>
#include <fstream>

using namespace std;

class TCalendario
{
	friend ostream& operator<<(ostream&, const TCalendario&);
	
	private:
		int dia, mes, anyo;
		char *mensaje;
		
		bool fechaCorrecta(int, int, int);
		bool esBisiesto(int);
  
	public:
		TCalendario();
		TCalendario(int, int, int, char*);
		TCalendario(const TCalendario &);
		~TCalendario();
		TCalendario& operator=(const TCalendario &);
    
		TCalendario operator+(int);
		TCalendario operator-(int);
		TCalendario operator++(int);
		TCalendario& operator++(void);
		TCalendario operator--(int);
		TCalendario& operator--(void);
    
		bool ModFecha(int, int, int);
		bool ModMensaje(char*);
		bool operator==(const TCalendario&) const;
		bool operator!=(const TCalendario&) const;
		bool operator>(const TCalendario&) const;
		bool operator<(const TCalendario&) const;
		bool EsVacio();
    
		int Dia();
		int Mes();
		int Anyo();
		char *Mensaje();
};

#endif
