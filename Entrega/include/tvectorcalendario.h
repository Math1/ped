#ifndef __TVECTORCALENDARIO_H_
#define __TVECTORCALENDARIO_H_

#include <iostream>

using namespace std;

#include "tcalendario.h"

class TVectorCalendario
{
	friend ostream& operator<<(ostream&, const TVectorCalendario &);
	
	private:
		TCalendario *c;
		int tamano;
		TCalendario error;	

	public:
		TVectorCalendario();
		TVectorCalendario(const int);
		TVectorCalendario(const TVectorCalendario&);
		~TVectorCalendario();
		TVectorCalendario& operator=(const TVectorCalendario&);
		
		bool operator==(const TVectorCalendario&);
		bool operator!=(const TVectorCalendario&);
		TCalendario& operator[](int);
		const TCalendario operator[](int) const;
		int Tamano();
		int Ocupadas();
		bool ExisteCal(TCalendario&);
		void MostrarMensajes(int, int, int);
		bool Redimensionar(int);
};	

#endif
