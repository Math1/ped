#include <queue>

#include "tabbcalendario.h"

TNodoABB::TNodoABB():item(), iz(), de() { }

TNodoABB::TNodoABB(const TNodoABB &n):item(n.item), iz(n.iz), de(n.de) { }

TNodoABB::~TNodoABB()
{

}

TNodoABB& TNodoABB::operator=(const TNodoABB &n)
{
	item = n.item;
	iz = n.iz;
	de = n.de;
	
	return *this;
}

void TABBCalendario::InordenAux(TVectorCalendario &v, int &pos) const
{
	if(!EsVacio())
	{
		raiz -> iz.InordenAux(v, pos);
		
		v[pos] = raiz -> item;
		
		pos++;
		
		raiz -> de.InordenAux(v, pos);
	}
}

void TABBCalendario::PreordenAux(TVectorCalendario &v, int &pos) const
{
	if(!EsVacio())
	{
		v[pos] = raiz -> item;
		
		pos++;
		
		raiz -> iz.PreordenAux(v, pos);
		
		raiz -> de.PreordenAux(v, pos);
	}
}

void TABBCalendario::PostordenAux(TVectorCalendario &v, int &pos) const
{
	if(!EsVacio())
	{
		raiz -> iz.PostordenAux(v, pos);
		
		raiz -> de.PostordenAux(v, pos);
		
		v[pos] = raiz -> item;
		
		pos++;
	}
}

TABBCalendario::TABBCalendario()
{
	raiz = NULL;
}

TABBCalendario::TABBCalendario(const TABBCalendario &arb)
{
	int nodos = arb.Nodos();
	raiz = NULL;
	TVectorCalendario v(arb.Preorden());
	
	for(int i = 1; i <= nodos; i++)
	{
		Insertar(v[i]);
	}
}

TABBCalendario::~TABBCalendario()
{
	if(raiz != NULL)
	{
		delete raiz;
		raiz = NULL;
	}
}

TABBCalendario & TABBCalendario::operator=(const TABBCalendario &arb)
{
	this -> ~TABBCalendario();
	
	int nodos = arb.Nodos();
	TVectorCalendario v(arb.Preorden());
	
	for(int i = 1; i <= nodos; i++)
	{
		Insertar(v[i]);
	}
		
	return *this;
}
		
bool TABBCalendario::operator==(const TABBCalendario &arb)
{
	TVectorCalendario v(arb.Inorden());
	
	for(int i = 1; i <= arb.Nodos(); i++)
	{
		if(!Buscar(v[i]))
			return false;
	}
	
	return true;
}

bool TABBCalendario::EsVacio() const
{
	if(raiz == NULL)
		return true;
	else
		return false;
}

bool TABBCalendario::Insertar(const TCalendario &c)
{
	if(!this -> EsVacio())
	{
		if(!Buscar(c))
		{
			if(c < raiz -> item)
			{
				return raiz -> iz.Insertar(c);
			}
			else
				return raiz -> de.Insertar(c);
		}
		else
			return false;
	}
	else
	{
		raiz = new TNodoABB();
		raiz -> item = c;
		
		return true;
	}
}

bool TABBCalendario::Borrar(const TCalendario &c)
{
	if(!EsVacio())
	{
		if(Buscar(c))
		{
			BorrarAux(c);
			
			return true;
		}
		else
			return false;
	}
	else
		return false;
}

void TABBCalendario::BorrarAux(const TCalendario &c)
{
	if(raiz == NULL)
		return;
		
	if(raiz -> item < c)
		raiz -> de.BorrarAux(c);
		
	else if(raiz -> item > c)
		raiz -> iz.BorrarAux(c);
		
	else
	{
		BorrarPadre();
	}
}

void TABBCalendario::BorrarPadre()
{
	TNodoABB *aux;
	aux = raiz;
	
	if(raiz -> iz.EsVacio())
	{
		raiz = raiz -> de.raiz;
		aux -> de.raiz = NULL;
		delete aux;
		aux = NULL;
	}
	else if(raiz -> de.EsVacio())
	{
		raiz = raiz -> iz.raiz;
		aux -> iz.raiz = NULL;
		delete aux;
		aux = NULL;
	}
	else
	{
		TNodoABB *aux;
		aux = raiz;

		if(raiz -> iz.raiz == NULL)
		{
			raiz = raiz -> de.raiz;
			aux -> de.raiz = NULL;
			delete aux;
			aux = NULL;
		}
		else if(raiz -> de.raiz == NULL)
		{
			raiz = raiz -> iz.raiz;
			aux -> iz.raiz = NULL;
			delete aux;
			aux = NULL;
		}
			else
			{
				BorrarPadreDosHijos();
			}
		}
}

void TABBCalendario::BorrarPadreDosHijos()
{
	TNodoABB *anterior, *actual, *siguiente;

	actual = raiz;
	anterior = raiz;
	siguiente = raiz -> iz.raiz;

	if(siguiente -> de.raiz  == NULL)
	{
		actual -> item = siguiente -> item;
		actual -> iz.raiz = siguiente -> iz.raiz;
	}
	else
	{
		while(siguiente -> de.raiz != NULL)
		{
			anterior = siguiente;
			siguiente = siguiente -> de.raiz;
		}
		
		anterior -> de.raiz = siguiente -> iz.raiz;
		actual -> item = siguiente -> item;
		siguiente -> iz.raiz = NULL;
		siguiente -> de.raiz = NULL;
	}
	siguiente = NULL;
	actual = NULL;
	anterior = NULL;
}

bool TABBCalendario::Buscar(const TCalendario &c) const
{
	if(raiz == NULL)
		return false;
	else if(raiz -> item == c)
		return true;
	else	
		return (raiz -> iz).Buscar(c) || (raiz -> de).Buscar(c); 	
}

TCalendario TABBCalendario::Raiz() const
{
	TCalendario c;
	
	if(raiz != NULL)
		return raiz -> item;
	else
		return c;
}

int TABBCalendario::Altura() const
{
	if(raiz == NULL)
		return 0;
	else
		return 1 + max(raiz -> iz.Altura(), raiz -> de.Altura());
}

int TABBCalendario::Nodos() const
{
	if(raiz == NULL)
		return 0;
	else 	
		return 1 + (raiz -> iz).Nodos() + (raiz -> de).Nodos();
}

int TABBCalendario::NodosHoja() const
{
	if((*this).EsVacio())
		return 0;
	
	if(raiz -> iz.raiz == NULL && raiz -> de.raiz == NULL)
		return 1;
	else if (raiz -> iz.raiz == NULL)
		return raiz -> de.NodosHoja();
	else if (raiz -> de.raiz == NULL)
		return raiz -> iz.NodosHoja();
	else
		return raiz -> iz.NodosHoja() + raiz -> de.NodosHoja();
}

TVectorCalendario TABBCalendario::Inorden() const
{
	int posicion = 1;
	TVectorCalendario v(Nodos());
	InordenAux(v, posicion);
	return v;
}

TVectorCalendario TABBCalendario::Preorden() const
{
	int posicion = 1;
	TVectorCalendario v(Nodos());
	PreordenAux(v, posicion);
	return v;
}

TVectorCalendario TABBCalendario::Postorden() const
{
	int posicion = 1;
	TVectorCalendario v(Nodos());
	PostordenAux(v, posicion);
	return v;
}

TVectorCalendario TABBCalendario::Niveles() const
{
	int nodos = Nodos();
	queue<TABBCalendario> queue;
	TABBCalendario aux;
	TVectorCalendario v;
	int pos = 1;
	
	if(!(*this).EsVacio())
	{
		v.Redimensionar(nodos);
		
		queue.push(*this);
		
		while(!queue.empty())
		{
			aux = queue.front();
			v[pos] = aux.raiz -> item;
			pos++;
			
			queue.pop();
			
			if(!aux.raiz -> iz.EsVacio())
				queue.push(aux.raiz -> iz);
			
			if(!aux.raiz -> de.EsVacio())
				queue.push(aux.raiz -> de);
		}
	}
	
	return v;
}

TABBCalendario TABBCalendario::operator+(TABBCalendario &arb)
{
	TABBCalendario nuevo(*this);
	TVectorCalendario v(arb.Inorden());
	
	for(int i = 1; i <= arb.Nodos(); i++)
		nuevo.Insertar(v[i]);
	
	return nuevo;
}

TABBCalendario TABBCalendario::operator-(TABBCalendario &arb)
{
	TABBCalendario nuevo;
	TVectorCalendario v(Inorden());
	
	for(int i = 1; i <= v.Ocupadas(); i++)
	{
		if(!arb.Buscar(v[i]))
			nuevo.Insertar(v[i]);
	}
	
	return nuevo;
}

int TABBCalendario::max(const int iz, const int de) const
{
	if(iz == de || iz > de)
		return iz;
	else
		return de;
}

ostream & operator<<(ostream &os, const TABBCalendario &arb)
{
	os << arb.Niveles();
	
	return os;
}
