#include <cstring>
#include <stdlib.h>

#include "tcalendario.h"

bool TCalendario::esBisiesto(int a)
{
	return (a % 4 == 0) && (a % 100 != 0 || a % 400 == 0);
}

bool TCalendario::fechaCorrecta(int d, int m, int a)
{
	int diasPorMes[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	
	if(a >= 1900)
	{
		if(d >= 1 && m <= 12 && m >= 1)
		{
			if(esBisiesto(a) && m == 2)
				return d <= diasPorMes[m - 1] + 1;
			else
				return d <= diasPorMes[m - 1];
		}
	}
	
	return false;
}

TCalendario::TCalendario()
{
	dia = mes = 1;
	anyo = 1900;
	mensaje = NULL;
}

TCalendario::TCalendario(int dia, int mes, int anyo, char *mens)
{
	if(fechaCorrecta(dia, mes, anyo))
	{
		this -> dia = dia;
		this -> mes = mes;
		this -> anyo = anyo;
	
		if(mens != NULL)
		{
			mensaje = new char[strlen(mens) + 1];
			mensaje = strcpy(mensaje, mens);
		}
		else
		{
			mensaje = NULL;
		}
	}
	else
	{
		this -> dia = this -> mes = 1;
		this -> anyo = 1900;
		this -> mensaje = NULL;
	}
}

TCalendario::TCalendario(const TCalendario &c)
{
	this -> dia = c.dia;
	this -> mes = c.mes;
	this -> anyo = c.anyo;
	
	if(c.mensaje != NULL)
	{
		mensaje = new char[strlen(c.mensaje) + 1];	
		mensaje = strcpy(mensaje, c.mensaje);
	}
	else
	{
		mensaje = NULL;
		return;
	}
}

TCalendario::~TCalendario()
{
	dia = mes = 1;
	anyo = 1900;
	
	if(mensaje != NULL)
	{
		delete []mensaje;
		mensaje = NULL;
	}
}

TCalendario& TCalendario::operator=(const TCalendario &c)
{
	if(this != &c)
	{
		dia = c.dia;
		mes = c.mes;
		anyo = c.anyo;
		
		if(mensaje != NULL)
			delete []mensaje;
		
		if(c.mensaje != NULL)
		{
			mensaje = new char[strlen(c.mensaje) + 1];	
			mensaje = strcpy(mensaje, c.mensaje);
		}
		else
			mensaje = NULL;
	}
	
	return *this;
}
    
TCalendario TCalendario::operator+(const int n)
{	
	TCalendario res = *this;

	if(n >= 0)
	{
		for(int i = 0; i < n; i++)
		{
			res.operator++();
		}
	}
	
	return res;
}

TCalendario TCalendario::operator-(const int n)
{
	TCalendario res = *this;

	for(int i = 0; i < n; i++)
	{
		res.operator--();
	}	

	return res;
}

TCalendario TCalendario::operator++(int)
{
	TCalendario res(*this);

	*this = (*this).operator++();
	
	return res;
}

TCalendario& TCalendario::operator++(void)
{
	int diasPorMes[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	
	dia++;
	
	if(mes == 2)
	{
	        if(!esBisiesto(anyo) && dia == 29)
		{
			dia = 1;
			mes++;
		}
		else if(esBisiesto(anyo) && dia == 30)
		{
			dia = 1;
			mes++;
		}
	}
	else if(dia == diasPorMes[mes - 1] + 1)
	{
		dia = 1;
		mes++;
		
		if(mes == 13)
		{
			mes = 1;
			anyo++;
		}
	}
	
	return *this;
}

TCalendario TCalendario::operator--(int)
{
	TCalendario res(*this);	
	
	*this = (*this).operator--();

	return res;
}

TCalendario& TCalendario::operator--(void)
{
	int diasPorMes[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	dia--;
		
	if(dia == 0)
	{
		if(mes == 3)
		{
			if(!esBisiesto(anyo))
			{
				mes--;
				dia = diasPorMes[mes - 1];
			}
			else
			{
				mes--;
				dia = diasPorMes[mes - 1] + 1;
			}
		}
		else
		{
			mes--;
			
			if(mes == 0)
			{
				if(anyo > 1900)
				{
					mes = 12;
					dia = diasPorMes[mes - 1];
					anyo--;
				}
				else
				{
					(*this).~TCalendario();
					
					return *this;
				}
			}
			else
			{
				dia = diasPorMes[mes - 1];
			}
		}
	}

	return *this;		
}
    
bool TCalendario::ModFecha(int d, int m, int a)
{
	if(fechaCorrecta(d, m , a))
	{
		dia = d;
		mes = m;
		anyo = a;
		
		return true;
	}
	else
		return false;
}

bool TCalendario::ModMensaje(char* mens)
{
	if(mens != NULL)
	{
		mensaje = new char[strlen(mens) + 1];
		strcpy(mensaje, mens);
	}
	else
	{	
		delete mensaje;
		mensaje = NULL;
		return true;
	}
	
	return false;
}

bool TCalendario::operator==(const TCalendario &other) const
{
	if(dia == other.dia && mes == other.mes && anyo == other.anyo)
	{
		if(mensaje != NULL && other.mensaje != NULL && strcmp(mensaje, other.mensaje) == 0)
		{
			return true;
		}
		else if(mensaje == NULL && other.mensaje == NULL)
			return true;
	}
	
	return false;
}

bool TCalendario::operator!=(const TCalendario &other) const
{
	return !(*this == other);
}

bool TCalendario::operator>(const TCalendario &c) const
{
	if(anyo > c.anyo)
		return true;
	else if(anyo == c.anyo)
	{
		if(mes > c.mes)
			return true;
		else if(mes == c.mes)
		{
			if(dia > c.dia)
				return true;
			else if(dia == c.dia)
			{
				if(mensaje != NULL && c.mensaje == NULL)
					return true;
				else if(mensaje == NULL)
					return false;
					
				return strcmp(mensaje, c.mensaje);
			}
			else
				return false;
		}
		else
			return false;
	}
	else
		return false;
}

bool TCalendario::operator<(const TCalendario &c) const
{
	if(*this == c)
		return false;
	
	return !(*this > c);
}

bool TCalendario::EsVacio()
{
	if(dia == 1 && mes == 1 && anyo == 1900 && mensaje == NULL)
		return true;
	else
		return false;
}
    
int TCalendario::Dia()
{
	return this -> dia;
}

int TCalendario::Mes()
{
	return this -> mes;
}

int TCalendario::Anyo()
{
	return this -> anyo;
}

char* TCalendario::Mensaje()
{
	return this -> mensaje;
}

ostream& operator<<(ostream &os, const TCalendario &c)
{
	if(c.dia <= 9)
		os << "0" << c.dia << "/";
	else
		os << c.dia << "/";
		
	if(c.mes <= 9)
		os << "0" << c.mes << "/";
	else
		os << c.mes << "/";
		
	os << c.anyo << " ";
	
	if(c.mensaje == NULL)
		os << "\"\"";
	else
		os << "\"" << c.mensaje << "\"";
		
	return os;
}
