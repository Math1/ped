#include <cmath>

#include "tavlcalendario.h"

TNodoAVL::TNodoAVL():item(), iz(), de() { fe = 0; }

TNodoAVL::TNodoAVL(const TNodoAVL &nodo):item(nodo.item), iz(nodo.iz), de(nodo.de) { fe = nodo.fe; }

TNodoAVL::~TNodoAVL() {}


TNodoAVL& TNodoAVL::operator=(const TNodoAVL &nodo)
{
	this -> item = nodo.item;
	this -> iz = nodo.iz;
	this -> de = nodo.de;
	this -> fe = nodo.fe;

	return *this;
}

ostream & operator<<(ostream &os, TAVLCalendario &arb)
{
	os << arb.Inorden();
	return os;
}

void TAVLCalendario::InordenAux(TVectorCalendario &v, int &pos) const
{
	if(!EsVacio())
	{
		raiz -> iz.InordenAux(v, pos);
		
		v[pos] = raiz -> item;
		
		pos++;
		
		raiz -> de.InordenAux(v, pos);
	}
}

void TAVLCalendario::PreordenAux(TVectorCalendario &v, int &pos) const
{
	if(!EsVacio())
	{
		v[pos] = raiz -> item;
		
		pos++;
		
		raiz -> iz.PreordenAux(v, pos);
		
		raiz -> de.PreordenAux(v, pos);
	}
}

void TAVLCalendario::PostordenAux(TVectorCalendario &v, int &pos) const
{
	if(!EsVacio())
	{
		raiz -> iz.PostordenAux(v, pos);
		
		raiz -> de.PostordenAux(v, pos);
		
		v[pos] = raiz -> item;
		
		pos++;
	}
}

int TAVLCalendario::max(int x, int y) const
{
	if(x >= y) return x;
	else return y;
}

TAVLCalendario::TAVLCalendario()
{
	raiz = NULL;
}	

TAVLCalendario::TAVLCalendario(const TAVLCalendario &arb)
{
	int nodos = arb.Nodos();
	raiz = NULL;
	TVectorCalendario v(arb.Preorden());
	
	for(int i = 1; i <= nodos; i++)
	{
		Insertar(v[i]);
	}
}

TAVLCalendario::~TAVLCalendario()
{
	if(raiz != NULL)
	{
		delete raiz;
		raiz = NULL;
	}
}

TAVLCalendario& TAVLCalendario::operator=(const TAVLCalendario &arb)
{
	this -> ~TAVLCalendario();

	int nodos = arb.Nodos();
	TVectorCalendario v(arb.Preorden());

	for(int i = 1; i <= nodos; i++)
		this -> Insertar(v[i]);

	return *this;
}

bool TAVLCalendario::operator==(const TAVLCalendario &arb)
{
	int nodos = arb.Nodos();
	TVectorCalendario v(arb.Inorden());

	for(int i = 1; i <= nodos; i++)
	{
		if(!Buscar(v[i]))
			return false;
	}

	return true;
}

bool TAVLCalendario::operator!=(const TAVLCalendario &arb)
{	
	return !(*this).operator==(arb);
}

bool TAVLCalendario::EsVacio() const
{
	if(raiz == NULL)
		return true;
	else
		return false;
}

bool TAVLCalendario::Insertar(const TCalendario &cal)
{	
	if(!Buscar(cal))
	{
		InsertarAux(cal);

		ActualizarPesos();

		while(!ComprobarPesos())
		{
			Reorganizar();

			ActualizarPesos();
		}
		
		return true;
	}
	else
		return false;
}

void TAVLCalendario::InsertarAux(const TCalendario &cal)
{
	if(!this -> EsVacio())
	{
		if(!Buscar(cal))
		{
			if(cal < raiz -> item)
			{
				raiz -> iz.InsertarAux(cal);
			}
			else
				raiz -> de.InsertarAux(cal);
		}
	}
	else
	{
		raiz = new TNodoAVL();
		raiz -> item = cal;
	}
}

void TAVLCalendario::ActualizarPesos()
{
	if(!EsVacio())
	{
		raiz -> fe = raiz -> de.Altura() - raiz -> iz.Altura();
		raiz -> iz.ActualizarPesos();
		raiz -> de.ActualizarPesos();
	}
}

bool TAVLCalendario::ComprobarPesos()
{
	if(EsVacio())
		return true;

	return abs(raiz -> fe) < 2 && raiz -> iz.ComprobarPesos() && raiz -> de.ComprobarPesos(); 
}

void TAVLCalendario::Reorganizar()
{
	TNodoAVL *aux = raiz;
	TNodoAVL *nodo;

	if(raiz != NULL)
	{
		if(abs(raiz -> fe) == 2)
		{
			if(raiz -> fe == 2)
			{
				nodo = raiz -> de.raiz;

				if(abs(nodo -> fe) != 2)
				{
					if(nodo -> fe == 1 || nodo -> fe == 0)
					{
						TNodoAVL *me_voy(nodo -> iz.raiz);

						aux -> de.raiz = me_voy;

						nodo -> iz.raiz = aux;

						raiz = nodo;

						//cout << "Rotacion DD" << endl; 
					}
					else if(nodo -> fe == -1)
					{
						TNodoAVL *nuevo_padre(nodo -> iz.raiz);

						aux -> de.raiz = NULL;
						nodo -> iz.raiz = NULL;

						if(nuevo_padre -> de.raiz != NULL)
							nodo -> iz.raiz = nuevo_padre -> de.raiz;

						if(nuevo_padre -> iz.raiz != NULL)
							aux -> de.raiz = nuevo_padre -> iz.raiz;

						nuevo_padre -> iz.raiz = aux;

						nuevo_padre -> de.raiz = nodo;

						raiz = nuevo_padre;

						//cout << "Rotacion DI" << endl;
					}
					else
						cout << "Tenemos un problema derecho" << endl;
				}
				else
					raiz -> de.Reorganizar();
			}
			else
			{
				nodo = raiz -> iz.raiz;

				if(abs(nodo -> fe) != 2)
				{
					if(nodo -> fe == 1)
					{
						TNodoAVL *nuevo_padre(nodo -> de.raiz);

						aux -> iz.raiz = NULL;
						nodo -> de.raiz = NULL;

						if(nuevo_padre -> iz.raiz != NULL)
							nodo -> de.raiz = nuevo_padre -> iz.raiz;		

						if(nuevo_padre -> de.raiz != NULL)
							aux -> iz.raiz = nuevo_padre -> de.raiz;

						nuevo_padre -> de.raiz = aux;

						nuevo_padre -> iz.raiz = nodo;
						
						raiz = nuevo_padre;

						//cout << "Rotacion ID" << endl;
					}
					else if(nodo -> fe == -1 || nodo -> fe == 0)
					{	
						TNodoAVL *me_voy(nodo -> de.raiz);

						aux -> iz.raiz = me_voy;

						nodo -> de.raiz = aux;

						raiz = nodo;
						
						//cout << "Rotacion II" << endl;
					}
					else
						cout << "Tenemos un problema izquierdo" << endl;
				}
				else
					raiz -> iz.Reorganizar();
			}
		}
		else
		{
			raiz -> iz.Reorganizar();
			raiz -> de.Reorganizar();
		}
	}
}

bool TAVLCalendario::Buscar(const TCalendario &cal)
{
	TVectorCalendario v(Inorden());

	return v.ExisteCal(cal);
}	

int TAVLCalendario::Altura() const
{
	if(raiz == NULL)
		return 0;
	else
		return 1 + max(raiz -> iz.Altura(), raiz -> de.Altura());
}

int TAVLCalendario::Nodos() const
{
	if(raiz == NULL)
		return 0;
	else 	
		return 1 + (raiz -> iz).Nodos() + (raiz -> de).Nodos();}

int TAVLCalendario::NodosHoja() const
{
	if((*this).EsVacio())
		return 0;
	
	if(raiz -> iz.raiz == NULL && raiz -> de.raiz == NULL)
		return 1;
	else if (raiz -> iz.raiz == NULL)
		return raiz -> de.NodosHoja();
	else if (raiz -> de.raiz == NULL)
		return raiz -> iz.NodosHoja();
	else
		return raiz -> iz.NodosHoja() + raiz -> de.NodosHoja();
}

TVectorCalendario TAVLCalendario::Inorden() const 
{
	int posicion = 1;
	TVectorCalendario v(Nodos());
	InordenAux(v, posicion);
	return v;
}

TVectorCalendario TAVLCalendario::Preorden() const
{
	int posicion = 1;
	TVectorCalendario v(Nodos());
	PreordenAux(v, posicion);
	return v;
}

TVectorCalendario TAVLCalendario::Postorden() const
{
	int posicion = 1;
	TVectorCalendario v(Nodos());
	PostordenAux(v, posicion);
	return v;
}

bool TAVLCalendario::Borrar(const TCalendario &c)
{
	if(!EsVacio())
	{
		if(Buscar(c))
		{
			BorrarAux(c);

			ActualizarPesos();

			while(!ComprobarPesos())
			{
				Reorganizar();

				ActualizarPesos();
			}
			return true;
		}
		else
			return false;
	}
	else
		return false;
}

void TAVLCalendario::BorrarAux(const TCalendario &c)
{
	if(raiz == NULL)
		return;
		
	if(raiz -> item < c)
		raiz -> de.BorrarAux(c);
		
	else if(raiz -> item > c)
		raiz -> iz.BorrarAux(c);
		
	else
	{
		BorrarPadre();
	}
}

void TAVLCalendario::BorrarPadre()
{
	TNodoAVL *aux;
	aux = raiz;
	
	if(raiz -> iz.EsVacio())
	{
		raiz = raiz -> de.raiz;
		aux -> de.raiz = NULL;
		delete aux;
		aux = NULL;
	}
	else if(raiz -> de.EsVacio())
	{
		raiz = raiz -> iz.raiz;
		aux -> iz.raiz = NULL;
		delete aux;
		aux = NULL;
	}
	else
		BorrarPadreDosHijos();
}

void TAVLCalendario::BorrarPadreDosHijos()
{
	TNodoAVL *anterior, *actual, *siguiente;

	actual = raiz;
	anterior = raiz;
	siguiente = raiz -> iz.raiz;

	if(siguiente -> de.raiz  == NULL)
	{
		actual -> item = siguiente -> item;
		actual -> iz.raiz = siguiente -> iz.raiz;
	}
	else
	{
		while(siguiente -> de.raiz != NULL)
		{
			anterior = siguiente;
			siguiente = siguiente -> de.raiz;
		}
		
		anterior -> de.raiz = siguiente -> iz.raiz;
		actual -> item = siguiente -> item;
		siguiente -> iz.raiz = NULL;
		siguiente -> de.raiz = NULL;
	}
	siguiente = NULL;
	actual = NULL;
	anterior = NULL;
}

TCalendario TAVLCalendario::Raiz() const
{
	TCalendario c;
	if(raiz != NULL) return raiz -> item;
	else return c;
}