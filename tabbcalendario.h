#ifndef _TABBCALENDARIO_H_
#define _TABBCALENDARIO_H_

#include <iostream>

using namespace std;

#include "tvectorcalendario.h"

class TNodoABB;

class TABBCalendario
{
	friend ostream & operator<<(ostream &, const TABBCalendario &);
	
	private:
		TNodoABB *raiz;
		void InordenAux(TVectorCalendario&, int&) const;
		void PreordenAux(TVectorCalendario&, int&) const;
		void PostordenAux(TVectorCalendario&, int&) const;
		int max(const int, const int) const;
		void BorrarAux(const TCalendario&);
		void BorrarPadre();
		void BorrarPadreDosHijos();
		
	public:
		TABBCalendario();
		TABBCalendario(const TABBCalendario&);
		~TABBCalendario();
		TABBCalendario &operator=(const TABBCalendario&);
		
		bool operator==(const TABBCalendario&);
		bool EsVacio() const;
		bool Insertar(const TCalendario&);
		bool Borrar(const TCalendario&);
		bool Buscar(const TCalendario&) const;
		TCalendario Raiz() const;
		int Altura() const;
		int Nodos() const;
		int NodosHoja() const;
		TVectorCalendario Inorden() const;
		TVectorCalendario Preorden() const;
		TVectorCalendario Postorden() const;
		TVectorCalendario Niveles() const;
		TABBCalendario operator+(TABBCalendario&);
		TABBCalendario operator-(TABBCalendario&);
};


class TNodoABB
{
	friend class TABBCalendario;
	
	private:
		TCalendario item;
		TABBCalendario iz, de;
		
	public:
		TNodoABB();
		TNodoABB(const TNodoABB&);
		~TNodoABB();
		TNodoABB &operator=(const TNodoABB&);
};


#endif
