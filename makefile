CC=g++
CFLAGS=-Wall -g
SOURCES=main.cpp tcalendario.cpp tvectorcalendario.cpp tlistacalendario.cpp tabbcalendario.cpp tavlcalendario.cpp
OBJS=$(SOURCES: .cpp=.o)
EXECUTABLE=main

$(EXECUTABLE): $(OBJS) 
	$(CC) $(CFLAGS) $(OBJS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f $(OBJS)
