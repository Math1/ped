#include <cstring>

#include "tvectorcalendario.h"

TVectorCalendario::TVectorCalendario()
{
	this -> c = NULL;
	this -> tamano = 0;
}
		
TVectorCalendario::TVectorCalendario(const int tam)
{
	if(tam > 0)
	{
		tamano = tam;
		c = new TCalendario[tam];
	}
	else
	{
		c = NULL;
		tamano = 0;
	}
}
		
TVectorCalendario::TVectorCalendario(const TVectorCalendario &v)
{		
	c = new TCalendario[v.tamano];
	
	for(int i = 0; i < v.tamano; i++)
	{
		c[i] = v.c[i];
	}
	
	tamano = v.tamano;
}
		
TVectorCalendario::~TVectorCalendario()
{
	if(c != NULL)
	{
		delete[] c;
		c = NULL;
	}
	
	tamano = 0;
}
		
TVectorCalendario& TVectorCalendario::operator=(const TVectorCalendario &v)
{
	if(*this != v)
	{
		(*this).~TVectorCalendario();

		tamano = v.tamano;

		if(tamano > 0)
		{
			c = new TCalendario[tamano];
			
			for(int i = 0; i < tamano; i++)
			{
				c[i] = v.c[i];
			}
		}
	}

	return *this;
}
		
bool TVectorCalendario::operator==(const TVectorCalendario &v)
{
	if(tamano == v.tamano)
	{
		for(int i = 0; i < tamano; i++)
		{
			if(c[i] != v.c[i])
				return false;
		}
		
		return true;
	}
	
	return false;
}
		
bool TVectorCalendario::operator!=(const TVectorCalendario &v)
{
	return !(*this == v);
}
		
TCalendario& TVectorCalendario::operator[](int pos)
{
	TCalendario *aux;
	
	if(pos < 1 || pos > tamano)
		aux = &error;
	else
		aux = &c[pos - 1];
	
	return *aux;
}
		
const TCalendario TVectorCalendario::operator[](int pos) const
{
	if(pos < 1 || pos > tamano)
		return error;
	else
		return c[pos - 1];
}
		
int TVectorCalendario::Tamano()
{
	return tamano;
}
		
int TVectorCalendario::Ocupadas()
{
	int cantidad = 0;
	
	for(int i = 0; i < tamano; i++)
	{
		if(!c[i].EsVacio())
			cantidad++;
	}
	
	return cantidad;
}
		
bool TVectorCalendario::ExisteCal(const TCalendario &cal)
{
	for(int i = 0; i < tamano; i++)
	{
		if(c[i] == cal)
		{
			return true;
		}
	}
	return false;
}
		
void TVectorCalendario::MostrarMensajes(int d, int m, int y)
{
	
	TCalendario c_temp(d, m, y, (char *) " ");
	bool no_primero = false;
	
	cout << "[";  

	if(!c_temp.EsVacio())
	{
		c_temp.ModMensaje(NULL);
		
		for(int i = 0; i < tamano; i++)
		{	
			if(c_temp < c[i])
			{
				if(no_primero)
					cout << ", ";
				cout << c[i];
				no_primero = true;
			}
		}
	}
	
	cout << "]";
}
		
bool TVectorCalendario::Redimensionar(int tam)
{
	if(tamano == tam)
		return false;
	
	TCalendario *temp = NULL;
	
	if(tam > 0 && tamano > 0)
	{	
		temp = new TCalendario[tam];
		
		int menor = 0;

		if(tamano < tam)
			menor = tamano;
		else
			menor = tam;

		for(int i = 0; i < menor; i++)
		{
			temp[i] = c[i];
		}
		
		delete [] c;

		c = new TCalendario[tam];
		
		for(int i = 0; i < menor; i++)
		{
			c[i] = temp[i];
		}

		tamano = tam;		

		delete [] temp;

		return true;
	}
	else if(tam > 0 && tamano == 0)
	{
		c = new TCalendario[tam];
		tamano = tam;
		
		return true;
	}
	else
		return false;
}

ostream& operator<<(ostream& os, const TVectorCalendario& v)
{
	bool primero = true;
	
	if(v.tamano == 0)
		os << "[]";
	else
	{
		os << "[";
		
		for(int i = 0; i < v.tamano; i++)
		{
			if(/*!v.c[i].EsVacio() && */!primero)
			{
				os << ", (" << i+1 << ") " << v.c[i];
			}
			if(/*!v.c[i].EsVacio() &&*/ primero)
			{
				os << "(" << i+1 << ") " << v.c[i];
				primero = false;
			}
		}
		
		os << "]";
	}
	
	return os;
}
